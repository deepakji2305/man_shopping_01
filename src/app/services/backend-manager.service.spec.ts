import { TestBed } from '@angular/core/testing';

import { BackendManagerService } from './backend-manager.service';

describe('BackendManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendManagerService = TestBed.get(BackendManagerService);
    expect(service).toBeTruthy();
  });
});
