import { Injectable } from '@angular/core';

import { Router } from '@angular/router';
import { BackendManagerService } from "./backend-manager.service";



@Injectable({
  providedIn: 'root'
})
export class ShoppingDataManagerService {

  public ProductData: any = [];

  public shoppingCart: any = [];
  public totalQty = 0;
  public totalPrice = 0;
  public discAmount = 0;
  public finalPrice = 0;
  public shippingCost = 0 ;
  public isDiscApplied = false;
  public appliedDiscount: any;
  public freeShippingDisc = false


  constructor(private backendService: BackendManagerService, ) {
    this.assignProductdata(); }


  assignProductdata(){
    this.ProductData = this.backendService.getShoppingData();
  }

  addtoCart(product) {

    let toCart = {
      id: product.id,
      qty: product.qty,
      price: product.price,
      name: product.name,
      imageUrl: product.imageUrl,
      itemtotPrice: product.qty * product.price,
      cat: product.categories
    };

    let extProd = this.shoppingCart.filter(cart => {
      return cart.id === toCart.id
    });

    if (extProd.length === 0) {
      this.shoppingCart.push(toCart);
    } else {
      extProd[0].qty = extProd[0].qty + toCart.qty
    }

    this.calculatePrice();
  }


  deleteCart(cart) {
    let idx = this.shoppingCart.findIndex(obj => {
      return obj.id === cart.id
    });
    this.shoppingCart.splice(idx, 1);
    this.calculatePrice();
  }

  calculatePrice() {
    this.totalPrice = 0;
    this.totalQty = 0;
    this.shippingCost = 0 ;
    
   
    this.shoppingCart.forEach(cart => {
  
      this.totalPrice = this.totalPrice + (cart.qty * cart.price);
      this.totalQty = this.totalQty + 1;      
     if(this.isDiscApplied){
       let result =  cart.cat.filter(cat=>{
         return cat === this.appliedDiscount.cat                
        });
     if(result.length > 0 ){
        this.discAmount =  this.discAmount + (cart.itemtotPrice * (this.appliedDiscount.perc /100))
     }
     }
    });

    if(!this.freeShippingDisc ){
      if(this.totalPrice  < 20 ){
        this.shippingCost = 7 ;
      } else if(this.totalPrice  < 40 ){
        this.shippingCost = 5 ;
      } 
    }
  

    this.finalPrice = this.totalPrice  - this.discAmount ;
    this.finalPrice =   this.finalPrice + this.shippingCost ;

   


  }


  placeOrder() {
    this.shoppingCart = [];
    this.totalQty = 0;
    this.totalPrice = 0;
    this.discAmount = 0;
    this.finalPrice  = 0 ;
    this.isDiscApplied = false;
  }

  validateDisc(discCode) {

    if(discCode.toUpperCase() === 'FREESHIPPING!'){
      this.freeShippingDisc = true ;
     return true ;
    }

    let result = this.backendService.disCountCodes.filter(code => {
      return code.code === discCode.toUpperCase();
    });

    if (result.length > 0) {
      this.appliedDiscount = result[0];
      return true;
    } else {
      return false;
    }

  }

  onApplyDiscount(discCode) {
    if(!this.freeShippingDisc){
      this.isDiscApplied = true ;
    }  
    this.calculatePrice() ;
    alert("Discount Applied successfully")
  }

}
