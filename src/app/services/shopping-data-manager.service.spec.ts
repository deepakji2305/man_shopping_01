import { TestBed } from '@angular/core/testing';

import { ShoppingDataManagerService } from './shopping-data-manager.service';

describe('ShoppingDataManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShoppingDataManagerService = TestBed.get(ShoppingDataManagerService);
    expect(service).toBeTruthy();
  });
});
