import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainpageComponent } from './pages/mainpage/mainpage.component';
import { ShoppinglistComponent } from './pages/shoppinglist/shoppinglist.component';



const appRoutes: Routes = [
  { path: '', component: MainpageComponent },
  { path: 'ShoppingPage',      component: ShoppinglistComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes,
    { enableTracing: true } // <-- debugging purposes only
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
