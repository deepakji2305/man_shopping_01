import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainpageComponent } from './pages/mainpage/mainpage.component';
import { ShoppinglistComponent } from './pages/shoppinglist/shoppinglist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BackendManagerService } from "./services/backend-manager.service";
import { ShoppingDataManagerService } from "./services/shopping-data-manager.service";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import {
  HttpModule,
  BaseRequestOptions
} from "@angular/http";


@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    ShoppinglistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,  
    HttpClientModule
   
       ],
  providers: [
    BackendManagerService,
    ShoppingDataManagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
