import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainpageComponent } from './mainpage.component';
import { Injectable, NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';

import{ RouterTestingModule} from '@angular/router/testing' ;
 

import { ShoppingDataManagerService } from "../../services/shopping-data-manager.service";
import { BackendManagerService } from "../../services/backend-manager.service";

import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';

let routerLink : Router;

let dataService : ShoppingDataManagerService ;
//let backendService : BackendManagerService ;
let component: MainpageComponent;
let fixture: ComponentFixture<MainpageComponent>;
let products : any [] ;

class ShoppingDataManagerServiceMock{
  //assignProductdata(ProductData){
    ProductData = [
    {
      "id": 1,
      "name": "USB Cable",
      "imageUrl": "https://images.unsplash.com/photo-1492107376256-4026437926cd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&h=400&q=80",
      "supplierId": 1,
      "wholesalePrice": 2,
      "price": 4,
      "categories": [
        "accessory"
      ]
    },
    {
      "id": 2,
      "name": "Laptop",
      "imageUrl": "https://images.unsplash.com/photo-1541807084-5c52b6b3adef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&h=400&q=80",
      "supplierId": 2,
      "wholesalePrice": 800,
      "price": 1000,
      "categories": [
        "electronic"
      ]
    },
    {
      "id": 3,
      "name": "Monitor",
      "imageUrl": "https://images.unsplash.com/photo-1546538915-a9e2c8d0a0b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&h=400&q=80",
      "supplierId": 1,
      "wholesalePrice": 180,
      "price": 220,
      "categories": [
        "electronic"
      ]
    },
    {
      "id": 4,
      "name": "Headphones",
      "imageUrl": "https://images.unsplash.com/photo-1524678606370-a47ad25cb82a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&h=400&q=80",
      "supplierId": 1,
      "wholesalePrice": 20,
      "price": 30,
      "categories": [
        "accessory",
        "electronic",
        "audio"
      ]
    }
  ];
   totalQty = 0;
   totalPrice = 0;
   discAmount = 0;
   finalPrice = 0;
 // }
}

describe('MainpageComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
     imports:[FormsModule,BrowserModule,
      RouterTestingModule.withRoutes([]) ],
      declarations: [ MainpageComponent ],
      providers:[{provide:ShoppingDataManagerService, useClass:ShoppingDataManagerServiceMock}],
      schemas:[NO_ERRORS_SCHEMA]
    })
   fixture = TestBed.createComponent(MainpageComponent);
   routerLink = TestBed.get(Router);
   component = fixture.componentInstance;
   fixture.detectChanges();
  }));

  // beforeEach(() => {
  //   setupTest();
  //   fixture.detectChanges();
  // });

 // dataService = TestBed.get(ShoppingDataManagerService);
 // backendService= TestBed.get(BackendManagerService)


  it('should create', () => {
   expect(component).toBeTruthy();
     // expect(component).toBeDefined();
  });

  it('#Test 1: Initial basket quantity shoule be 0 ', () => {
    const qty : HTMLElement = getHTMLElement('#totalQtyId')
    expect(qty.textContent).toBe('Items: 0 Qty');
  });


  it('#Test 2: Initial basket amount shoule be 0 ', () => {
    const price : HTMLElement = getHTMLElement('#totalPriceId')
    expect(price.textContent).toBe('Total Price : £0.00');
  });


  it('#Test 3: Add first item to Cart', () => {
    let leaveButton : DebugElement = getDebugElement('#itemAddButton0');
    leaveButton.triggerEventHandler('click',null);
    fixture.detectChanges();   
  });



});


function getHTMLElement(selector : string): HTMLElement {
  console.log(fixture.nativeElement.querySelector(selector));
  return fixture.nativeElement.querySelector(selector);
}

function getDebugElement(cssSelector: string): DebugElement {
  return fixture.debugElement.query(By.css(cssSelector));
}