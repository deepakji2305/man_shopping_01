import { Component, OnInit } from '@angular/core';
import { ShoppingDataManagerService } from "../../services/shopping-data-manager.service";

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})
export class MainpageComponent implements OnInit {

  public products : any [] ;
  public  discCode = "" ;

  constructor( public  dataService: ShoppingDataManagerService) {

                        }

  ngOnInit() {

   this.products =  this.dataService.ProductData ;
   this.products.forEach(prod=>{prod.qty = 1})

  }

  increment(product) {
   if(product.qty >=1) product.qty++
  }

  decrement(product) {
    if(product.qty >1)   product.qty--
  }

  onAddtocart(product){
    this.dataService.addtoCart(product);
    product.qty = 1 ;
  }

  onApplyDiscount(){
    if (this.dataService.validateDisc(this.discCode)){
      this.dataService.onApplyDiscount(this.discCode);
    } else{
      alert("OOPS invalid discount code!!")
    }
    
  }

}
