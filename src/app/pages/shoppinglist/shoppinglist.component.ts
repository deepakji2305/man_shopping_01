import { Component, OnInit } from '@angular/core';

import { ShoppingDataManagerService } from "../../services/shopping-data-manager.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-shoppinglist',
  templateUrl: './shoppinglist.component.html',
  styleUrls: ['./shoppinglist.component.scss']
})
export class ShoppinglistComponent implements OnInit {

  constructor( public  dataService: ShoppingDataManagerService,
    public router: Router) { }

  ngOnInit() {
  }

  onDeleteCart(cart){
    this.dataService.deleteCart(cart)
  }

  onplaceOrder(){
    this.dataService.placeOrder();
    this.router.navigate(['/']);
    alert("Congradulations Your order has been places")
  }

}
