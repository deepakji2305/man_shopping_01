import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppinglistComponent } from './shoppinglist.component';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import{ RouterTestingModule} from '@angular/router/testing' ;


import { ShoppingDataManagerService } from "../../services/shopping-data-manager.service";
import {Router} from '@angular/router';


let routerLink : Router;
let ShoppingService : ShoppingDataManagerService ;


describe('ShoppinglistComponent', () => {
  let component: ShoppinglistComponent;
  let fixture: ComponentFixture<ShoppinglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ 
        FormsModule,BrowserModule,
        RouterTestingModule.withRoutes([])
      ] ,
      declarations: [ ShoppinglistComponent ],
      providers: [
        { provide: ShoppingService, useValue: ShoppingService }
        ]}        
        )
        fixture = TestBed.createComponent(ShoppinglistComponent);
        routerLink = TestBed.get(Router);
        component = fixture.componentInstance;
        fixture.detectChanges();


  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ShoppinglistComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
